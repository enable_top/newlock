﻿using UnityEngine;
using System.Collections;
using System;

public class ChestLock : MonoBehaviour {

    public String Saveowner;
    public String owner;
    public String keystatusy;
    public String keystatusn;

    public GameObject lockedch;
    public GameObject fixerch;
    public GameObject player;
    public string[] pnstatslp = { "NULL", "NULL", "NULL", "NULL", "NULL" };
    public int lpton;
    public float lpt;
    public int counter;
    public int antispam = 0;

    public int tcounton;
    public float tcount;

    static void _lockstatus(ref string[] arr)
    {
        arr = new string[5];
        arr[0] = "no";
        arr[1] = "no";
        arr[2] = "no";
        arr[3] = "no";
        arr[4] = "no";
    }

    void Start ()
    {
        player = GameObject.Find("PlayerU");
        lockedch = GameObject.Find("ntoggle");
        fixerch = GameObject.Find("ytoggle");

        //Таймер для основного времени взлома
        lpton = 0;
        lpt = 20;

        //Таймер для механики взлома
        tcounton = 0;
        tcount = 10;
        lockedch.gameObject.SetActive(false);
        fixerch.gameObject.SetActive(false);

        //Первичная инициализация массива

    }

    void Update()
    {
        //Условия основного таймера
        if (lpton == 1)
        {
            if (lpt > 0)
            {
                lpt -= Time.deltaTime;
            }
            if (lpt < 0)
            {
                lpcrash();
            }
        }

        //Условия механики таймера
        if (tcounton == 1)
        {
            if (tcount > 0)
            {
                tcount -= Time.deltaTime;
                _switchlocklp();
            }
            if (tcount < 0)
            {
                lpcrash();
            }
        }      
    }

    void OnMouseDown()
    {
        Debug.Log("Владелец : " + owner);
    }

    void OnTriggerStay()
    {      
        _lockstatus(ref pnstatslp);

        if (owner == "NULL")//слегка не работает (
        {
            owner = player.GetComponent<MainUser>().name;
            Saveowner = owner;
        }

        if (player.GetComponent<MainUser>().name == Saveowner)
        {
            Debug.Log("Открыто!");
        }
        else
        {
            if (Input.GetKeyUp(KeyCode.F))
            {
                Debug.Log("ЛОГ : Попытка взлома / вызов функции");
                lockpickopen();
                counter = 5;
                antispam++;
                switchlock();
            }
        }

        //Условие проверки массива (ВАЖНО!!! После всей готовности, УДАЛИТЬ)
        if (Input.GetKeyUp(KeyCode.C))
        {
            for (int i = 0; i < pnstatslp.Length; i++)
            {
                if (pnstatslp[i] == "no")
                {
                    Debug.Log("ЛОГ : Взлом возможен");
                }
                else
                {
                    Debug.Log("ЛОГ : Уже взломано");
                }
            }
        }
    }
    void lockpickopen()
    {
        if (player.GetComponent<MainUser>().lockpick > 0)
        {
            Debug.Log("ЛОГ : Активируем таймер");
            lpton = 1;
            tcounton = 1;
        }
    }

    void switchlock()
    {
        counter = 5;
        Debug.Log("ЛОГ : Текущее значение датчика - " + counter);      
    }

    void _switchlocklp()
    {
        _lockstatus(ref pnstatslp);

        if (counter != 0)
        {
            switch (counter)
            {
                case 5:
                    for (int i = 0; i < pnstatslp.Length; i++)
                    {
                        if (pnstatslp[i] == "no")
                        {
                            Debug.Log("ЛОГ : Взлом возможен");
                        }
                        else
                        {
                            Debug.Log("ЛОГ : Уже взломано");
                        }
                    }
                    break;              
                default:
                    print("ОШИБКА : Что то с датчиком не то");
                    break;
            }
        }
    }

    void lpcrash()
    {
        Debug.Log("ЛОГ : Отмычка сломана!!!");
        lpton = 0;
        lpt = 20;
        tcounton = 0;
        tcount = 10;
    }
}
